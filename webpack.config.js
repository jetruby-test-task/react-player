const ExtractTextPlugin = require('extract-text-webpack-plugin');
const getSlug = require('speakingurl');
const glob = require('glob-all');
const naturalSort = require('javascript-natural-sort');
const webpack = require('webpack');

/*
|-------------------------------------------------------------------------------
| Вспомогательные функции
|-------------------------------------------------------------------------------
*/

/**
 * Проверка, является ли модуль вендором
 * @param {String} module
 * @returns {Boolean}
 */
function isExternal(module) {
  const request = module.userRequest;

  if (typeof request !== 'string') return false;

  return (
    request.indexOf('bower_components') >= 0 ||
    request.indexOf('node_modules') >= 0
  );
}

/**
 * Формирование объекта из названий страниц и ссылок на них.
 * @returns {[String]}
 */
function getPagesObject() {
  const pagesUrls = glob
    .sync(['./src/pages/**'], {
      nodir: true,
      matchBase: false
    })
    .sort(naturalSort)
    .map(element =>
      getSlug(
        element.replace(/\.\/src\/pages\//, '').replace(/\.htm/, '.html'),
        {
          mark: true,
          uric: true
        }
      )
    );

  const pagesNames = glob
    .sync(['./src/pages/**'], {
      nodir: true,
      matchBase: false
    })
    .sort(naturalSort)
    .map(element =>
      element.replace(/\.\/src\/pages\//, '').replace(/\.htm/, '')
    );

  const pagesObject = pagesUrls.reduce(
    (accumulator, value, index) =>
      Object.assign(accumulator, {
        [value]: pagesNames[index]
      }),
    {}
  );

  return pagesObject;
}

/*
|-------------------------------------------------------------------------------
| Конфигурация webpack
|-------------------------------------------------------------------------------
*/
const config = {
  context: __dirname,

  resolve: {
    alias: {
      masonry: 'masonry-layout',
      isotope: 'isotope-layout'
    },
    modules: ['bower_components', 'node_modules'],
    extensions: ['.js', '.jsx']
  },

  entry: {
    // app: glob.sync([
    //   './src/partials/**/*.js',
    //   '!./src/partials/app/*.js',
    //   '!./src/partials/**/*.test.js',
    //   '!./src/partials/**/*.spec.js',
    //   './src/partials/app/*.js'
    // ])
    app: './src/partials/app/app.jsx'
  },

  output: {
    path: `${__dirname}/WEBPACK_TEST`,
    filename: '[name].js'
  },

  module: {
    rules: [
      {
        test: /\.jsx?$/,
        exclude: /(node_modules(?!\/(vue-clicky))|bower_components)/,
        use: {
          loader: 'babel-loader'
        }
      },
      {
        test: /\.(tpl|htm)$/,
        use: [
          {
            loader: 'html-loader',
            options: {
              minimize: true,
              removeComments: false,
              collapseWhitespace: true,
              removeAttributeQuotes: false,
              caseSensitive: true,
              customAttrSurround: [
                [/#/, /(?:)/],
                [/\*/, /(?:)/],
                [/\[?\(?/, /(?:)/]
              ],
              customAttrAssign: [/\)?\]?=/]
            }
          }
        ]
      },
      {
        test: /\.md$/,
        use: [
          {
            loader: 'html-loader'
          },
          {
            loader: 'markdown-loader',
            options: {}
          }
        ]
      },
      {
        test: /\.css$/,
        loader: ExtractTextPlugin.extract({
          fallback: 'style-loader',
          use: [
            {
              loader:
                process.env.NODE_ENV === 'dev'
                  ? 'css-loader?sourceMap'
                  : 'css-loader',
              options: {
                minimize: process.env.NODE_ENV !== 'dev'
              }
            }
          ]
        })
      },
      {
        test: /\.(eot|svg|ttf|woff|woff2)(\?\S*)?$/,
        loader: 'file-loader',
        query: {
          name: '../fonts/[name].[ext]'
        }
      },
      {
        test: /\.php$/,
        loader: 'file-loader',
        query: {
          name: '../forms/[name].[ext]'
        }
      },
      {
        test: /\.(png|jpe?g|gif|svg)(\?\S*)?$/,
        loader: 'file-loader',
        query: {
          name: '../images/[name].[ext]'
        }
      }
    ]
  },

  plugins: [
    // new webpack.ProvidePlugin({
    //   $: 'jquery',
    //   jQuery: 'jquery'
    // }),

    new ExtractTextPlugin('../styles/vendor.css'),

    new webpack.optimize.CommonsChunkPlugin({
      name: 'vendor',
      minChunks(module) {
        return isExternal(module);
      }
    }),

    new webpack.DefinePlugin({
      'process.env': {
        NODE_ENV: JSON.stringify(process.env.NODE_ENV),
        PROXY: JSON.stringify(process.env.PROXY),
        STYLEGUIDE: {
          pagesObject: JSON.stringify(getPagesObject())
        }
      }
    })
  ],

  devtool: process.env.NODE_ENV === 'dev' ? 'source-map' : false,

  watch: process.env.NODE_ENV === 'dev',

  watchOptions: {
    aggregateTimeout: 300
  }
};

if (process.env.NODE_ENV === 'build') {
  config.plugins.push(
    new webpack.optimize.UglifyJsPlugin({
      compress: {
        warnings: false,
        drop_console: true
      }
    })
  );
}

module.exports = config;
