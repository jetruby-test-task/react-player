webpackJsonp([0],{

/***/ 254:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = __webpack_require__(43);

var _react2 = _interopRequireDefault(_react);

var _reactDom = __webpack_require__(257);

var _reactDom2 = _interopRequireDefault(_reactDom);

var _bowser2 = __webpack_require__(272);

var _bowser3 = _interopRequireDefault(_bowser2);

__webpack_require__(274);

__webpack_require__(479);

var _playerContainer = __webpack_require__(480);

var _playerContainer2 = _interopRequireDefault(_playerContainer);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; } /* eslint  no-unused-vars: 0 */

var cq = __webpack_require__(500)({ preprocess: true });

var App = function (_React$Component) {
  _inherits(App, _React$Component);

  function App(props) {
    _classCallCheck(this, App);

    var _this = _possibleConstructorReturn(this, (App.__proto__ || Object.getPrototypeOf(App)).call(this, props));

    _this.state = {
      device: {},
      noScroll: false
    };
    _this.bowser = _this.bowser.bind(_this);
    return _this;
  }

  _createClass(App, [{
    key: 'componentWillMount',
    value: function componentWillMount() {
      this.bowser();
    }
  }, {
    key: 'bowser',
    value: function bowser() {
      function detectBrowserEngine() {
        var browser = void 0;
        if (_bowser3.default.blink) {
          browser = 'blink';
        } else if (_bowser3.default.webkit) {
          browser = 'webkit';
        } else if (_bowser3.default.gecko) {
          browser = 'gecko';
        } else if (_bowser3.default.msie) {
          browser = 'msie';
        } else if (_bowser3.default.msedge) {
          browser = 'msedge';
        }
        return browser;
      }

      function detectDeviceType() {
        var deviceType = void 0;
        if (_bowser3.default.mobile) {
          deviceType = 'mobile';
        } else if (_bowser3.default.tablet) {
          deviceType = 'tablet';
        } else {
          deviceType = 'laptop';
        }
        return deviceType;
      }

      function detectOs() {
        var os = void 0;
        if (_bowser3.default.mac) {
          os = 'mac';
        } else if (_bowser3.default.windows) {
          os = 'windows';
        } else if (_bowser3.default.windowsphone) {
          os = 'windowsphone';
        } else if (_bowser3.default.linux) {
          os = 'linux';
        } else if (_bowser3.default.chromeos) {
          os = 'chromeos';
        } else if (_bowser3.default.android) {
          os = 'android';
        } else if (_bowser3.default.ios) {
          os = 'ios';
        } else if (_bowser3.default.blackberry) {
          os = 'blackberry';
        } else if (_bowser3.default.firefoxos) {
          os = 'firefoxos';
        } else if (_bowser3.default.webos) {
          os = 'webos';
        } else if (_bowser3.default.bada) {
          os = 'bada';
        } else if (_bowser3.default.tizen) {
          os = 'tizen';
        } else if (_bowser3.default.sailfish) {
          os = 'sailfish';
        }
        return os;
      }

      this.setState({
        device: {
          browser: _bowser3.default.name,
          browserVersion: _bowser3.default.version,
          browserEngine: detectBrowserEngine(),
          type: detectDeviceType(),
          os: detectOs(),
          osVersion: _bowser3.default.osversion
        }
      });
    }
  }, {
    key: 'render',
    value: function render() {
      return _react2.default.createElement(
        'div',
        {
          className: 'app',
          id: 'app',
          'data-browser': this.state.device.browser,
          'data-browser-version': this.state.device.browserVersion,
          'data-browser-engine': this.state.device.browserEngine,
          'data-device-type': this.state.device.type,
          'data-os': this.state.device.os,
          'data-os-version': this.state.device.osVersion,
          'data-no-scroll': this.state.noScroll
        },
        _react2.default.createElement(_playerContainer2.default, null)
      );
    }
  }]);

  return App;
}(_react2.default.Component);

_reactDom2.default.render(_react2.default.createElement(App, null), document.getElementById('root'));

/***/ }),

/***/ 480:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = __webpack_require__(43);

var _react2 = _interopRequireDefault(_react);

var _classnames = __webpack_require__(97);

var _classnames2 = _interopRequireDefault(_classnames);

var _jsmediatags = __webpack_require__(490);

var _jsmediatags2 = _interopRequireDefault(_jsmediatags);

var _playerControls = __webpack_require__(481);

var _playerControls2 = _interopRequireDefault(_playerControls);

var _playerSearch = __webpack_require__(488);

var _playerSearch2 = _interopRequireDefault(_playerSearch);

var _playerTracklist = __webpack_require__(489);

var _playerTracklist2 = _interopRequireDefault(_playerTracklist);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _toConsumableArray(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = Array(arr.length); i < arr.length; i++) { arr2[i] = arr[i]; } return arr2; } else { return Array.from(arr); } }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; } /* eslint react/sort-comp: 0 */


var PlayerContainer = function (_React$Component) {
  _inherits(PlayerContainer, _React$Component);

  function PlayerContainer(props) {
    _classCallCheck(this, PlayerContainer);

    var _this = _possibleConstructorReturn(this, (PlayerContainer.__proto__ || Object.getPrototypeOf(PlayerContainer)).call(this, props));

    _this.state = {
      controlsState: {
        el: null,
        src: null,
        playState: false,
        volume: 0.6,
        currentTime: 0,
        userSetTime: null,
        duration: null,
        mute: false,
        lastVolume: null
      },
      searchState: {
        isFocusOnSearch: false,
        query: ''
      },
      tracklistState: {
        currentTrackId: null,
        tracklist: __webpack_require__(499),
        scrollbar: undefined
      }
    };
    _this.searchMethods = {
      setFocusOnSearchState: _this.setFocusOnSearchState.bind(_this),
      updateQuery: _this.updateQuery.bind(_this),
      clearQuery: _this.clearQuery.bind(_this)
    };
    _this.tracklistMethods = {
      setCurrentTrackId: _this.setCurrentTrackId.bind(_this),
      getMetadata: _this.getMetadata.bind(_this)
    };
    _this.controlsMethods = {
      setAudio: _this.setAudio.bind(_this),
      setAudioState: _this.setAudioState.bind(_this)
    };
    return _this;
  }

  /*
  |-------------------------------------------------------------------------------
  | Controls
  |-------------------------------------------------------------------------------
  */

  /**
   * Создание элемента "аудио"
   * @param {*} payload
   */


  _createClass(PlayerContainer, [{
    key: 'setAudio',
    value: function setAudio() {
      var audioElement = document.getElementById('player-controls-audio');
      this.setState({
        controlsState: _extends({}, this.state.controlsState, {
          el: audioElement
        })
      });
      audioElement.volume = this.state.controlsState.volume;
    }

    /**
     * Изменение состояния трека
     * @param {Object} payload
     */

  }, {
    key: 'setAudioState',
    value: function setAudioState(payload) {
      this.setState({
        controlsState: _extends({}, this.state.controlsState, payload)
      });
    }

    /*
    |-------------------------------------------------------------------------------
    | Search
    |-------------------------------------------------------------------------------
    */

    /**
     * Установка состояния поискового компонента активен/неактивен
     * @param {*} state
     * @param {Boolean} focus
     */

  }, {
    key: 'setFocusOnSearchState',
    value: function setFocusOnSearchState(focus) {
      this.setState({
        searchState: _extends({}, this.state.searchState, {
          isFocusOnSearch: focus
        })
      });
    }

    /**
     * Обновление запроса на сортировку
     * @param {*} event
     */

  }, {
    key: 'updateQuery',
    value: function updateQuery(event) {
      this.setState({
        searchState: _extends({}, this.state.searchState, {
          query: event.target.value
        })
      });
    }

    /**
     * Очистка запроса на сортировку
     * @param {*} state
     */

  }, {
    key: 'clearQuery',
    value: function clearQuery() {
      this.setState({
        searchState: _extends({}, this.state.searchState, {
          isFocusOnSearch: false,
          query: ''
        })
      });
    }

    /*
    |-------------------------------------------------------------------------------
    | Tracklist
    |-------------------------------------------------------------------------------
    */

    /**
     * Установка id текущего трека
     * @param {Number} index - индекс трека
     */

  }, {
    key: 'setCurrentTrackId',
    value: function setCurrentTrackId(index) {
      this.setState({
        tracklistState: _extends({}, this.state.tracklistState, {
          currentTrackId: index
        })
      });
    }

    /**
     * Получение метаданных из треков
     *
     * TODO: убрать дублирование треков
     */

  }, {
    key: 'getMetadata',
    value: function getMetadata() {
      var _this2 = this;

      var tracklistWithMetadata = this.state.tracklistState.tracklist;
      tracklistWithMetadata.forEach(function (item, index) {
        new Promise(function (resolve, reject) {
          new _jsmediatags2.default.Reader(document.location.href + item.link).read({
            onSuccess: function onSuccess(response) {
              item.metadata = response;
              resolve(response);
            },
            onError: function onError(error) {
              reject(error);
            }
          });
        }).then(function () {
          _this2.setState({
            tracklistState: _extends({}, _this2.state.tracklistState, {
              tracklist: tracklistWithMetadata
            })
          });
        }).then(function () {
          var audio = document.createElement('audio');
          audio.setAttribute('src', item.link);
          audio.addEventListener('loadedmetadata', function () {
            var currentItem = _this2.state.tracklistState.tracklist[index];
            currentItem.duration = audio.duration;
            _this2.setState({
              tracklistState: _extends({}, _this2.state.tracklistState, {
                tracklist: [].concat(_toConsumableArray(_this2.state.tracklistState.tracklist), [currentItem])
              })
            });
          });
        }).catch(function (error) {
          console.log(error);
        });
      });
    }
  }, {
    key: 'render',
    value: function render() {
      return _react2.default.createElement(
        'section',
        { className: 'player-container' },
        _react2.default.createElement(
          'header',
          {
            className: (0, _classnames2.default)({
              'player-container__controls-wrap': true,
              'player-container__controls-wrap--hidden': this.state.searchState.isFocusOnSearch
            })
          },
          _react2.default.createElement(_playerControls2.default, {
            controlsState: this.state.controlsState,
            controlsMethods: this.controlsMethods,
            tracklistState: this.state.tracklistState,
            tracklistMethods: this.tracklistMethods
          })
        ),
        _react2.default.createElement(
          'section',
          {
            className: (0, _classnames2.default)({
              'player-container__search-wrap': true,
              'player-container__search-wrap--active': this.state.searchState.isFocusOnSearch
            })
          },
          _react2.default.createElement(_playerSearch2.default, {
            searchState: this.state.searchState,
            searchMethods: this.searchMethods
          })
        ),
        _react2.default.createElement(
          'main',
          { className: 'player-container__tracklist-wrap' },
          _react2.default.createElement(_playerTracklist2.default, {
            tracklistState: this.state.tracklistState,
            tracklistMethods: this.tracklistMethods,
            searchState: this.state.searchState,
            searchMethods: this.searchMethods,
            controlsState: this.state.controlsState,
            controlsMethods: this.controlsMethods
          })
        )
      );
    }
  }]);

  return PlayerContainer;
}(_react2.default.Component);

exports.default = PlayerContainer;

/***/ }),

/***/ 481:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = __webpack_require__(43);

var _react2 = _interopRequireDefault(_react);

var _propTypes = __webpack_require__(133);

var _propTypes2 = _interopRequireDefault(_propTypes);

var _classnames = __webpack_require__(97);

var _classnames2 = _interopRequireDefault(_classnames);

var _exportsLoaderParticlesJSWindowParticlesJSWindowPJSDomParticles = __webpack_require__(484);

var _exportsLoaderParticlesJSWindowParticlesJSWindowPJSDomParticles2 = _interopRequireDefault(_exportsLoaderParticlesJSWindowParticlesJSWindowPJSDomParticles);

var _moment = __webpack_require__(1);

var _moment2 = _interopRequireDefault(_moment);

__webpack_require__(253);

var _playerControls = __webpack_require__(487);

var _playerControls2 = _interopRequireDefault(_playerControls);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; } /* eslint 'import/no-extraneous-dependencies': 0 */
/* eslint 'import/no-webpack-loader-syntax': 0 */
/* eslint 'import/extensions': 0 */
/* eslint 'import/no-unresolved': 0 */
/* eslint 'react/forbid-prop-types': 0 */


var PlayerControls = function (_React$Component) {
  _inherits(PlayerControls, _React$Component);

  function PlayerControls(props) {
    _classCallCheck(this, PlayerControls);

    var _this = _possibleConstructorReturn(this, (PlayerControls.__proto__ || Object.getPrototypeOf(PlayerControls)).call(this, props));

    _this.state = {
      plugins: {
        particles: {
          enable: true,
          options: _playerControls2.default
        }
      }
    };
    _this.changeTrack = _this.changeTrack.bind(_this);
    return _this;
  }

  _createClass(PlayerControls, [{
    key: 'componentDidMount',
    value: function componentDidMount() {
      // Создание аудиоэлемента
      this.props.controlsMethods.setAudio({
        el: document.getElementById('player-controls-audio')
      });

      // Инициализация декора "частицы"
      if (this.state.plugins.particles.enable) {
        _exportsLoaderParticlesJSWindowParticlesJSWindowPJSDomParticles2.default.particlesJS('player-controls', this.state.plugins.particles.options);
      }
    }

    /**
     * Смена трека в плейлисте
     * @param {'next'|'prev'} direction - следующий/предыдущий
     */

  }, {
    key: 'changeTrack',
    value: function changeTrack(direction) {
      if (direction === 'prev') {
        if (this.props.tracklistState.currentTrackId === null) {
          this.props.tracklistMethods.setCurrentTrackId(this.props.tracklistState.tracklist.length - 1);
        } else if (this.currentTrackId <= this.props.tracklistState.tracklist.length - 1 && this.currentTrackId > 0) {
          this.props.tracklistMethods.setCurrentTrackId(this.props.tracklistState.currentTrackId - 1);
        } else {
          this.props.tracklistMethods.setCurrentTrackId(this.props.tracklistState.tracklist.length - 1);
        }
      } else if (direction === 'next') {
        if (this.props.tracklistState.currentTrackId === null) {
          this.props.tracklistMethods.setCurrentTrackId(0);
        } else if (this.props.tracklistState.currentTrackId < this.props.tracklistState.tracklist.length - 1) {
          this.props.tracklistMethods.setCurrentTrackId(this.props.tracklistState.currentTrackId + 1);
        } else {
          this.props.tracklistMethods.setCurrentTrackId(0);
        }
      } else {
        throw new Error('Передан неверный параметр');
      }
    }
  }, {
    key: 'render',
    value: function render() {
      var _this2 = this;

      return _react2.default.createElement(
        'section',
        {
          className: 'player-controls',
          id: 'player-controls',
          'data-particles': this.props.controlsState.playState
        },
        _react2.default.createElement(
          'audio',
          {
            src: this.props.controlsState.src,
            id: 'player-controls-audio',
            type: 'audio/mp3',
            preload: 'auto',
            onTimeUpdate: function onTimeUpdate() {
              return _this2.props.controlsMethods.setAudioState({
                currentTime: _this2.props.controlsState.el.currentTime
              });
            },
            hidden: true
          },
          _react2.default.createElement('track', { kind: 'captions' })
        ),
        _react2.default.createElement(
          'div',
          { className: 'player-controls__content-wrap' },
          _react2.default.createElement(
            'figure',
            {
              className: (0, _classnames2.default)({
                'player-controls__info': true,
                'player-controls__info--empty': this.props.tracklistState.currentTrackId === null
              })
            },
            _react2.default.createElement(
              'header',
              { className: 'player-controls__info-header' },
              _react2.default.createElement(
                'div',
                null,
                _react2.default.createElement(
                  'p',
                  { className: 'player-controls__info-header-artist', key: 'artist' },
                  'artist'
                ),
                _react2.default.createElement(
                  'p',
                  { className: 'player-controls__info-header-track', key: 'track' },
                  'title'
                )
              )
            )
          ),
          _react2.default.createElement(
            'nav',
            { className: 'player-controls__nav' },
            _react2.default.createElement(
              'div',
              { className: 'player-controls__nav-buttons-wrap' },
              _react2.default.createElement(
                'button',
                {
                  className: (0, _classnames2.default)({
                    'player-controls__nav-button': true,
                    'player-controls__nav-button--type--prev': true
                  }),
                  type: 'button'
                },
                'Prev'
              ),
              _react2.default.createElement(
                'button',
                {
                  className: (0, _classnames2.default)({
                    'player-controls__nav-button': true,
                    'player-controls__nav-button--type--play': true,
                    'player-controls__nav-button--type--pause': false
                  }),
                  type: 'button'
                },
                'Play'
              ),
              _react2.default.createElement(
                'button',
                {
                  className: (0, _classnames2.default)({
                    'player-controls__nav-button': true,
                    'player-controls__nav-button--type--next': true
                  }),
                  type: 'button'
                },
                'Next'
              )
            ),
            _react2.default.createElement(
              'div',
              { className: 'player-controls__nav-indicators-wrap' },
              _react2.default.createElement(
                'figure',
                { className: 'player-controls__diration' },
                _react2.default.createElement(
                  'span',
                  { className: 'player-controls__diration-count' },
                  '--:--'
                ),
                _react2.default.createElement('input', {
                  className: 'player-controls__range',
                  type: 'range',
                  min: '0',
                  max: '9',
                  step: '1'
                }),
                _react2.default.createElement(
                  'span',
                  { className: 'player-controls__diration-count' },
                  '--:--'
                )
              ),
              _react2.default.createElement(
                'figure',
                { className: 'player-controls__volume' },
                _react2.default.createElement('button', {
                  className: (0, _classnames2.default)({
                    'player-controls__volume-button': true,
                    'player-controls__volume-button--mute': false
                  }),
                  type: 'button'
                }),
                _react2.default.createElement('input', {
                  className: 'player-controls__range',
                  type: 'range',
                  min: '0',
                  max: '1',
                  'v-model': 'audio.volume',
                  step: 'any'
                })
              )
            )
          )
        )
      );
    }
  }]);

  return PlayerControls;
}(_react2.default.Component);

PlayerControls.propTypes = {
  controlsState: _propTypes2.default.object.isRequired,
  controlsMethods: _propTypes2.default.object.isRequired,
  tracklistState: _propTypes2.default.object.isRequired,
  tracklistMethods: _propTypes2.default.object.isRequired
};

exports.default = PlayerControls;

/***/ }),

/***/ 486:
/***/ (function(module, exports, __webpack_require__) {

var map = {
	"./af": 134,
	"./af.js": 134,
	"./ar": 135,
	"./ar-dz": 136,
	"./ar-dz.js": 136,
	"./ar-kw": 137,
	"./ar-kw.js": 137,
	"./ar-ly": 138,
	"./ar-ly.js": 138,
	"./ar-ma": 139,
	"./ar-ma.js": 139,
	"./ar-sa": 140,
	"./ar-sa.js": 140,
	"./ar-tn": 141,
	"./ar-tn.js": 141,
	"./ar.js": 135,
	"./az": 142,
	"./az.js": 142,
	"./be": 143,
	"./be.js": 143,
	"./bg": 144,
	"./bg.js": 144,
	"./bm": 145,
	"./bm.js": 145,
	"./bn": 146,
	"./bn.js": 146,
	"./bo": 147,
	"./bo.js": 147,
	"./br": 148,
	"./br.js": 148,
	"./bs": 149,
	"./bs.js": 149,
	"./ca": 150,
	"./ca.js": 150,
	"./cs": 151,
	"./cs.js": 151,
	"./cv": 152,
	"./cv.js": 152,
	"./cy": 153,
	"./cy.js": 153,
	"./da": 154,
	"./da.js": 154,
	"./de": 155,
	"./de-at": 156,
	"./de-at.js": 156,
	"./de-ch": 157,
	"./de-ch.js": 157,
	"./de.js": 155,
	"./dv": 158,
	"./dv.js": 158,
	"./el": 159,
	"./el.js": 159,
	"./en-au": 160,
	"./en-au.js": 160,
	"./en-ca": 161,
	"./en-ca.js": 161,
	"./en-gb": 162,
	"./en-gb.js": 162,
	"./en-ie": 163,
	"./en-ie.js": 163,
	"./en-nz": 164,
	"./en-nz.js": 164,
	"./eo": 165,
	"./eo.js": 165,
	"./es": 166,
	"./es-do": 167,
	"./es-do.js": 167,
	"./es-us": 168,
	"./es-us.js": 168,
	"./es.js": 166,
	"./et": 169,
	"./et.js": 169,
	"./eu": 170,
	"./eu.js": 170,
	"./fa": 171,
	"./fa.js": 171,
	"./fi": 172,
	"./fi.js": 172,
	"./fo": 173,
	"./fo.js": 173,
	"./fr": 174,
	"./fr-ca": 175,
	"./fr-ca.js": 175,
	"./fr-ch": 176,
	"./fr-ch.js": 176,
	"./fr.js": 174,
	"./fy": 177,
	"./fy.js": 177,
	"./gd": 178,
	"./gd.js": 178,
	"./gl": 179,
	"./gl.js": 179,
	"./gom-latn": 180,
	"./gom-latn.js": 180,
	"./gu": 181,
	"./gu.js": 181,
	"./he": 182,
	"./he.js": 182,
	"./hi": 183,
	"./hi.js": 183,
	"./hr": 184,
	"./hr.js": 184,
	"./hu": 185,
	"./hu.js": 185,
	"./hy-am": 186,
	"./hy-am.js": 186,
	"./id": 187,
	"./id.js": 187,
	"./is": 188,
	"./is.js": 188,
	"./it": 189,
	"./it.js": 189,
	"./ja": 190,
	"./ja.js": 190,
	"./jv": 191,
	"./jv.js": 191,
	"./ka": 192,
	"./ka.js": 192,
	"./kk": 193,
	"./kk.js": 193,
	"./km": 194,
	"./km.js": 194,
	"./kn": 195,
	"./kn.js": 195,
	"./ko": 196,
	"./ko.js": 196,
	"./ky": 197,
	"./ky.js": 197,
	"./lb": 198,
	"./lb.js": 198,
	"./lo": 199,
	"./lo.js": 199,
	"./lt": 200,
	"./lt.js": 200,
	"./lv": 201,
	"./lv.js": 201,
	"./me": 202,
	"./me.js": 202,
	"./mi": 203,
	"./mi.js": 203,
	"./mk": 204,
	"./mk.js": 204,
	"./ml": 205,
	"./ml.js": 205,
	"./mr": 206,
	"./mr.js": 206,
	"./ms": 207,
	"./ms-my": 208,
	"./ms-my.js": 208,
	"./ms.js": 207,
	"./mt": 209,
	"./mt.js": 209,
	"./my": 210,
	"./my.js": 210,
	"./nb": 211,
	"./nb.js": 211,
	"./ne": 212,
	"./ne.js": 212,
	"./nl": 213,
	"./nl-be": 214,
	"./nl-be.js": 214,
	"./nl.js": 213,
	"./nn": 215,
	"./nn.js": 215,
	"./pa-in": 216,
	"./pa-in.js": 216,
	"./pl": 217,
	"./pl.js": 217,
	"./pt": 218,
	"./pt-br": 219,
	"./pt-br.js": 219,
	"./pt.js": 218,
	"./ro": 220,
	"./ro.js": 220,
	"./ru": 221,
	"./ru.js": 221,
	"./sd": 222,
	"./sd.js": 222,
	"./se": 223,
	"./se.js": 223,
	"./si": 224,
	"./si.js": 224,
	"./sk": 225,
	"./sk.js": 225,
	"./sl": 226,
	"./sl.js": 226,
	"./sq": 227,
	"./sq.js": 227,
	"./sr": 228,
	"./sr-cyrl": 229,
	"./sr-cyrl.js": 229,
	"./sr.js": 228,
	"./ss": 230,
	"./ss.js": 230,
	"./sv": 231,
	"./sv.js": 231,
	"./sw": 232,
	"./sw.js": 232,
	"./ta": 233,
	"./ta.js": 233,
	"./te": 234,
	"./te.js": 234,
	"./tet": 235,
	"./tet.js": 235,
	"./th": 236,
	"./th.js": 236,
	"./tl-ph": 237,
	"./tl-ph.js": 237,
	"./tlh": 238,
	"./tlh.js": 238,
	"./tr": 239,
	"./tr.js": 239,
	"./tzl": 240,
	"./tzl.js": 240,
	"./tzm": 241,
	"./tzm-latn": 242,
	"./tzm-latn.js": 242,
	"./tzm.js": 241,
	"./uk": 243,
	"./uk.js": 243,
	"./ur": 244,
	"./ur.js": 244,
	"./uz": 245,
	"./uz-latn": 246,
	"./uz-latn.js": 246,
	"./uz.js": 245,
	"./vi": 247,
	"./vi.js": 247,
	"./x-pseudo": 248,
	"./x-pseudo.js": 248,
	"./yo": 249,
	"./yo.js": 249,
	"./zh-cn": 250,
	"./zh-cn.js": 250,
	"./zh-hk": 251,
	"./zh-hk.js": 251,
	"./zh-tw": 252,
	"./zh-tw.js": 252
};
function webpackContext(req) {
	return __webpack_require__(webpackContextResolve(req));
};
function webpackContextResolve(req) {
	var id = map[req];
	if(!(id + 1)) // check for number or string
		throw new Error("Cannot find module '" + req + "'.");
	return id;
};
webpackContext.keys = function webpackContextKeys() {
	return Object.keys(map);
};
webpackContext.resolve = webpackContextResolve;
module.exports = webpackContext;
webpackContext.id = 486;

/***/ }),

/***/ 487:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
var particlesConfig = {
  particles: {
    number: {
      value: 160,
      density: {
        enable: true,
        value_area: 800
      }
    },
    color: {
      value: '#ffffff'
    },
    shape: {
      type: 'circle',
      stroke: {
        width: 0,
        color: '#000000'
      },
      polygon: {
        nb_sides: 5
      },
      image: {
        src: 'img/github.svg',
        width: 0,
        height: 0
      }
    },
    opacity: {
      value: 0,
      random: false,
      anim: {
        enable: false,
        speed: 1,
        opacity_min: 0.1,
        sync: false
      }
    },
    size: {
      value: 0,
      random: false,
      anim: {
        enable: false,
        speed: 40,
        size_min: 0.1,
        sync: false
      }
    },
    line_linked: {
      enable: true,
      distance: 60,
      color: '#ffffff',
      opacity: 0.3,
      width: 1
    },
    move: {
      enable: true,
      speed: 3,
      direction: 'none',
      random: false,
      straight: false,
      out_mode: 'out',
      bounce: false,
      attract: {
        enable: false,
        rotateX: 600,
        rotateY: 1200
      }
    }
  },
  interactivity: {
    detect_on: 'canvas',
    events: {
      onhover: {
        enable: false,
        mode: 'repulse'
      },
      onclick: {
        enable: false,
        mode: 'push'
      },
      resize: true
    },
    modes: {
      grab: {
        distance: 400,
        line_linked: {
          opacity: 1
        }
      },
      bubble: {
        distance: 400,
        size: 40,
        duration: 2,
        opacity: 8,
        speed: 3
      },
      repulse: {
        distance: 200,
        duration: 0.4
      },
      push: {
        particles_nb: 4
      },
      remove: {
        particles_nb: 2
      }
    }
  },
  retina_detect: true
};

exports.default = particlesConfig;

/***/ }),

/***/ 488:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = __webpack_require__(43);

var _react2 = _interopRequireDefault(_react);

var _propTypes = __webpack_require__(133);

var _propTypes2 = _interopRequireDefault(_propTypes);

var _classnames = __webpack_require__(97);

var _classnames2 = _interopRequireDefault(_classnames);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; } /* eslint react/prefer-stateless-function: 0 */


var PlayerSearch = function (_React$Component) {
  _inherits(PlayerSearch, _React$Component);

  function PlayerSearch() {
    _classCallCheck(this, PlayerSearch);

    return _possibleConstructorReturn(this, (PlayerSearch.__proto__ || Object.getPrototypeOf(PlayerSearch)).apply(this, arguments));
  }

  _createClass(PlayerSearch, [{
    key: 'render',
    value: function render() {
      var _this2 = this;

      return _react2.default.createElement(
        'section',
        { className: 'player-search' },
        _react2.default.createElement(
          'div',
          { className: 'player-search__content-wrap' },
          _react2.default.createElement('input', {
            className: 'player-search__field',
            type: 'search',
            placeholder: 'Search for artists or tracks',
            onChange: this.props.searchMethods.updateQuery,
            value: this.props.searchState.query,
            onFocus: function onFocus() {
              _this2.props.searchMethods.setFocusOnSearchState(true);
            }
          }),
          _react2.default.createElement('button', {
            className: (0, _classnames2.default)({
              'player-search__button': true,
              'player-search__button--active': this.props.searchState.isFocusOnSearch
            }),
            type: 'button',
            onClick: function onClick() {
              _this2.props.searchMethods.clearQuery();
            }
          })
        )
      );
    }
  }]);

  return PlayerSearch;
}(_react2.default.Component);

PlayerSearch.propTypes = {
  searchState: _propTypes2.default.shape({
    isFocusOnSearch: _propTypes2.default.bool.isRequired,
    query: _propTypes2.default.string.isRequired
  }).isRequired,
  searchMethods: _propTypes2.default.shape({
    setFocusOnSearchState: _propTypes2.default.func.isRequired,
    updateQuery: _propTypes2.default.func.isRequired,
    clearQuery: _propTypes2.default.func.isRequired
  }).isRequired
};

exports.default = PlayerSearch;

/***/ }),

/***/ 489:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = __webpack_require__(43);

var _react2 = _interopRequireDefault(_react);

var _propTypes = __webpack_require__(133);

var _propTypes2 = _interopRequireDefault(_propTypes);

var _classnames = __webpack_require__(97);

var _classnames2 = _interopRequireDefault(_classnames);

var _moment = __webpack_require__(1);

var _moment2 = _interopRequireDefault(_moment);

var _perfectScrollbar = __webpack_require__(496);

var _perfectScrollbar2 = _interopRequireDefault(_perfectScrollbar);

__webpack_require__(497);

__webpack_require__(253);

__webpack_require__(498);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; } /* eslint react/forbid-prop-types: 0 */
/* eslint no-unused-vars: 0 */


var PlayerTracklist = function (_React$Component) {
  _inherits(PlayerTracklist, _React$Component);

  function PlayerTracklist(props) {
    _classCallCheck(this, PlayerTracklist);

    /**
     * Форматирование длительности трека
     * @param {Number} value длительность в мс
     */
    var _this = _possibleConstructorReturn(this, (PlayerTracklist.__proto__ || Object.getPrototypeOf(PlayerTracklist)).call(this, props));

    _this.formatDuration = function (value) {
      return _moment2.default.duration(parseInt(value, 10), 'seconds').format('mm:ss', {
        trim: false
      });
    };
    return _this;
  }

  _createClass(PlayerTracklist, [{
    key: 'componentWillMount',
    value: function componentWillMount() {
      this.props.tracklistMethods.getMetadata();
    }
  }, {
    key: 'componentDidMount',
    value: function componentDidMount() {
      var scrollbar = new _perfectScrollbar2.default('.player-tracklist[data-scrollbar]', {
        suppressScrollX: true
      });
    }
  }, {
    key: 'render',
    value: function render() {
      var _this2 = this;

      return _react2.default.createElement(
        'section',
        { className: 'player-tracklist', 'data-scrollbar': true },
        _react2.default.createElement(
          'figure',
          { className: 'player-tracklist__preloader' },
          _react2.default.createElement('span', { className: 'player-tracklist__preloader-image' }),
          _react2.default.createElement(
            'p',
            { className: 'player-tracklist__preloader-text' },
            'Loading...'
          )
        ),
        _react2.default.createElement(
          'ul',
          { className: 'player-tracklist__items' },
          this.props.tracklistState.tracklist.map(function (item) {
            return Object.keys(item.metadata).length ? _react2.default.createElement(
              'li',
              {
                className: (0, _classnames2.default)({
                  'player-tracklist__item': true,
                  'player-tracklist__item--active': false
                }),
                key: item.link
              },
              _react2.default.createElement(
                'span',
                { className: 'player-tracklist__item-artist' },
                item.metadata.tags.artist
              ),
              _react2.default.createElement(
                'span',
                { className: 'player-tracklist__item-title' },
                item.metadata.tags.title
              ),
              _react2.default.createElement(
                'span',
                { className: 'player-tracklist__item-duration' },
                _this2.formatDuration(item.duration)
              )
            ) : _react2.default.createElement('li', { key: item.link });
          })
        ),
        _react2.default.createElement(
          'figure',
          { className: 'player-tracklist__no-results', key: 'empty' },
          _react2.default.createElement(
            'span',
            { className: 'player-tracklist__no-results-text' },
            'Nothing found'
          )
        )
      );
    }
  }]);

  return PlayerTracklist;
}(_react2.default.Component);

PlayerTracklist.propTypes = {
  tracklistState: _propTypes2.default.object.isRequired,
  tracklistMethods: _propTypes2.default.object.isRequired,
  searchState: _propTypes2.default.object.isRequired,
  searchMethods: _propTypes2.default.object.isRequired,
  controlsState: _propTypes2.default.object.isRequired,
  controlsMethods: _propTypes2.default.object.isRequired
};

exports.default = PlayerTracklist;

/***/ }),

/***/ 499:
/***/ (function(module, exports) {

module.exports = [{"link":"assets/sounds/player-tracklist.1.mp3","metadata":{}},{"link":"assets/sounds/player-tracklist.2.mp3","metadata":{}},{"link":"assets/sounds/player-tracklist.3.mp3","metadata":{}},{"link":"assets/sounds/player-tracklist.4.mp3","metadata":{}},{"link":"assets/sounds/player-tracklist.5.mp3","metadata":{}},{"link":"assets/sounds/player-tracklist.6.mp3","metadata":{}},{"link":"assets/sounds/player-tracklist.7.mp3","metadata":{}},{"link":"assets/sounds/player-tracklist.8.mp3","metadata":{}},{"link":"assets/sounds/player-tracklist.9.mp3","metadata":{}},{"link":"assets/sounds/player-tracklist.10.mp3","metadata":{}}]

/***/ })

},[254]);
//# sourceMappingURL=app.js.map