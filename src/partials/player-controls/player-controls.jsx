/* eslint 'import/no-extraneous-dependencies': 0 */
/* eslint 'import/no-webpack-loader-syntax': 0 */
/* eslint 'import/extensions': 0 */
/* eslint 'import/no-unresolved': 0 */
/* eslint 'react/forbid-prop-types': 0 */
import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import particles from 'exports-loader?particlesJS=window.particlesJS,window.pJSDom!particles.js';
import moment from 'moment';
import 'moment-duration-format';
import particlesConfig from './player-controls.particles-config';

class PlayerControls extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      plugins: {
        particles: {
          enable: true,
          options: particlesConfig
        }
      }
    };
    this.changeTrack = this.changeTrack.bind(this);
  }

  componentDidMount() {
    // Создание аудиоэлемента
    this.props.controlsMethods.setAudio({
      el: document.getElementById('player-controls-audio')
    });

    // Инициализация декора "частицы"
    if (this.state.plugins.particles.enable) {
      particles.particlesJS(
        'player-controls',
        this.state.plugins.particles.options
      );
    }
  }

  /**
   * Смена трека в плейлисте
   * @param {'next'|'prev'} direction - следующий/предыдущий
   */
  changeTrack(direction) {
    if (direction === 'prev') {
      if (this.props.tracklistState.currentTrackId === null) {
        this.props.tracklistMethods.setCurrentTrackId(
          this.props.tracklistState.tracklist.length - 1
        );
      } else if (
        this.currentTrackId <= this.props.tracklistState.tracklist.length - 1 &&
        this.currentTrackId > 0
      ) {
        this.props.tracklistMethods.setCurrentTrackId(
          this.props.tracklistState.currentTrackId - 1
        );
      } else {
        this.props.tracklistMethods.setCurrentTrackId(
          this.props.tracklistState.tracklist.length - 1
        );
      }
    } else if (direction === 'next') {
      if (this.props.tracklistState.currentTrackId === null) {
        this.props.tracklistMethods.setCurrentTrackId(0);
      } else if (
        this.props.tracklistState.currentTrackId <
        this.props.tracklistState.tracklist.length - 1
      ) {
        this.props.tracklistMethods.setCurrentTrackId(
          this.props.tracklistState.currentTrackId + 1
        );
      } else {
        this.props.tracklistMethods.setCurrentTrackId(0);
      }
    } else {
      throw new Error('Передан неверный параметр');
    }
  }
  render() {
    return (
      <section
        className="player-controls"
        id="player-controls"
        data-particles={this.props.controlsState.playState}
      >
        <audio
          src={this.props.controlsState.src}
          id="player-controls-audio"
          type="audio/mp3"
          preload="auto"
          onTimeUpdate={() =>
            this.props.controlsMethods.setAudioState({
              currentTime: this.props.controlsState.el.currentTime
            })
          }
          hidden
        >
          <track kind="captions" />
        </audio>
        <div className="player-controls__content-wrap">
          <figure
            className={classNames({
              'player-controls__info': true,
              'player-controls__info--empty':
                this.props.tracklistState.currentTrackId === null
            })}
          >
            <header className="player-controls__info-header">
              <div>
                <p className="player-controls__info-header-artist" key="artist">
                  artist
                </p>
                <p className="player-controls__info-header-track" key="track">
                  title
                </p>
              </div>
            </header>
          </figure>
          <nav className="player-controls__nav">
            <div className="player-controls__nav-buttons-wrap">
              <button
                className={classNames({
                  'player-controls__nav-button': true,
                  'player-controls__nav-button--type--prev': true
                })}
                type="button"
              >
                Prev
              </button>
              <button
                className={classNames({
                  'player-controls__nav-button': true,
                  'player-controls__nav-button--type--play': true,
                  'player-controls__nav-button--type--pause': false
                })}
                type="button"
              >
                Play
              </button>
              <button
                className={classNames({
                  'player-controls__nav-button': true,
                  'player-controls__nav-button--type--next': true
                })}
                type="button"
              >
                Next
              </button>
            </div>
            <div className="player-controls__nav-indicators-wrap">
              <figure className="player-controls__diration">
                <span className="player-controls__diration-count">--:--</span>
                <input
                  className="player-controls__range"
                  type="range"
                  min="0"
                  max="9"
                  step="1"
                />
                <span className="player-controls__diration-count">--:--</span>
              </figure>
              <figure className="player-controls__volume">
                <button
                  className={classNames({
                    'player-controls__volume-button': true,
                    'player-controls__volume-button--mute': false
                  })}
                  type="button"
                />
                <input
                  className="player-controls__range"
                  type="range"
                  min="0"
                  max="1"
                  v-model="audio.volume"
                  step="any"
                />
              </figure>
            </div>
          </nav>
        </div>
      </section>
    );
  }
}

PlayerControls.propTypes = {
  controlsState: PropTypes.object.isRequired,
  controlsMethods: PropTypes.object.isRequired,
  tracklistState: PropTypes.object.isRequired,
  tracklistMethods: PropTypes.object.isRequired
};

export default PlayerControls;
