/* eslint react/forbid-prop-types: 0 */
/* eslint no-unused-vars: 0 */
import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import moment from 'moment';
import PerfectScrollbar from 'perfect-scrollbar';
import 'perfect-scrollbar/css/perfect-scrollbar.css';
import 'moment-duration-format';
import 'animate.css';

class PlayerTracklist extends React.Component {
  constructor(props) {
    super(props);
    /**
     * Форматирование длительности трека
     * @param {Number} value длительность в мс
     */
    this.formatDuration = value =>
      moment.duration(parseInt(value, 10), 'seconds').format('mm:ss', {
        trim: false
      });
  }

  componentWillMount() {
    this.props.tracklistMethods.getMetadata();
  }

  componentDidMount() {
    const scrollbar = new PerfectScrollbar(
      '.player-tracklist[data-scrollbar]',
      {
        suppressScrollX: true
      }
    );
  }

  render() {
    return (
      <section className="player-tracklist" data-scrollbar>
        <figure className="player-tracklist__preloader">
          <span className="player-tracklist__preloader-image" />
          <p className="player-tracklist__preloader-text">Loading...</p>
        </figure>
        <ul className="player-tracklist__items">
          {this.props.tracklistState.tracklist.map(
            item =>
              Object.keys(item.metadata).length ? (
                <li
                  className={classNames({
                    'player-tracklist__item': true,
                    'player-tracklist__item--active': false
                  })}
                  key={item.link}
                >
                  <span className="player-tracklist__item-artist">
                    {item.metadata.tags.artist}
                  </span>
                  <span className="player-tracklist__item-title">
                    {item.metadata.tags.title}
                  </span>
                  <span className="player-tracklist__item-duration">
                    {this.formatDuration(item.duration)}
                  </span>
                </li>
              ) : (
                <li key={item.link} />
              )
          )}
        </ul>
        <figure className="player-tracklist__no-results" key="empty">
          <span className="player-tracklist__no-results-text">
            Nothing found
          </span>
        </figure>
      </section>
    );
  }
}

PlayerTracklist.propTypes = {
  tracklistState: PropTypes.object.isRequired,
  tracklistMethods: PropTypes.object.isRequired,
  searchState: PropTypes.object.isRequired,
  searchMethods: PropTypes.object.isRequired,
  controlsState: PropTypes.object.isRequired,
  controlsMethods: PropTypes.object.isRequired
};

export default PlayerTracklist;
