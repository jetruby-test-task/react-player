/* eslint  no-unused-vars: 0 */

import React from 'react';
import ReactDOM from 'react-dom';
import bowser from 'bowser';
import 'babel-polyfill';
import 'normalize.css/normalize.css';
import PlayerContainer from '../player-container/player-container';

const cq = require('cq-prolyfill')({ preprocess: true });

class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      device: {},
      noScroll: false
    };
    this.bowser = this.bowser.bind(this);
  }

  componentWillMount() {
    this.bowser();
  }

  bowser() {
    function detectBrowserEngine() {
      let browser;
      if (bowser.blink) {
        browser = 'blink';
      } else if (bowser.webkit) {
        browser = 'webkit';
      } else if (bowser.gecko) {
        browser = 'gecko';
      } else if (bowser.msie) {
        browser = 'msie';
      } else if (bowser.msedge) {
        browser = 'msedge';
      }
      return browser;
    }

    function detectDeviceType() {
      let deviceType;
      if (bowser.mobile) {
        deviceType = 'mobile';
      } else if (bowser.tablet) {
        deviceType = 'tablet';
      } else {
        deviceType = 'laptop';
      }
      return deviceType;
    }

    function detectOs() {
      let os;
      if (bowser.mac) {
        os = 'mac';
      } else if (bowser.windows) {
        os = 'windows';
      } else if (bowser.windowsphone) {
        os = 'windowsphone';
      } else if (bowser.linux) {
        os = 'linux';
      } else if (bowser.chromeos) {
        os = 'chromeos';
      } else if (bowser.android) {
        os = 'android';
      } else if (bowser.ios) {
        os = 'ios';
      } else if (bowser.blackberry) {
        os = 'blackberry';
      } else if (bowser.firefoxos) {
        os = 'firefoxos';
      } else if (bowser.webos) {
        os = 'webos';
      } else if (bowser.bada) {
        os = 'bada';
      } else if (bowser.tizen) {
        os = 'tizen';
      } else if (bowser.sailfish) {
        os = 'sailfish';
      }
      return os;
    }

    this.setState({
      device: {
        browser: bowser.name,
        browserVersion: bowser.version,
        browserEngine: detectBrowserEngine(),
        type: detectDeviceType(),
        os: detectOs(),
        osVersion: bowser.osversion
      }
    });
  }

  render() {
    return (
      <div
        className="app"
        id="app"
        data-browser={this.state.device.browser}
        data-browser-version={this.state.device.browserVersion}
        data-browser-engine={this.state.device.browserEngine}
        data-device-type={this.state.device.type}
        data-os={this.state.device.os}
        data-os-version={this.state.device.osVersion}
        data-no-scroll={this.state.noScroll}
      >
        <PlayerContainer />
      </div>
    );
  }
}

ReactDOM.render(<App />, document.getElementById('root'));
