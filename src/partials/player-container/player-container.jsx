/* eslint react/sort-comp: 0 */
import React from 'react';
import classNames from 'classnames';
import jsmediatags from 'jsmediatags/dist/jsmediatags.min';
import PlayerControls from '../player-controls/player-controls';
import PlayerSearch from '../player-search/player-search';
import PlayerTracklist from '../player-tracklist/player-tracklist';

class PlayerContainer extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      controlsState: {
        el: null,
        src: null,
        playState: false,
        volume: 0.6,
        currentTime: 0,
        userSetTime: null,
        duration: null,
        mute: false,
        lastVolume: null
      },
      searchState: {
        isFocusOnSearch: false,
        query: ''
      },
      tracklistState: {
        currentTrackId: null,
        tracklist: require('../player-tracklist/player-tracklist.json'),
        scrollbar: undefined
      }
    };
    this.searchMethods = {
      setFocusOnSearchState: this.setFocusOnSearchState.bind(this),
      updateQuery: this.updateQuery.bind(this),
      clearQuery: this.clearQuery.bind(this)
    };
    this.tracklistMethods = {
      setCurrentTrackId: this.setCurrentTrackId.bind(this),
      getMetadata: this.getMetadata.bind(this)
    };
    this.controlsMethods = {
      setAudio: this.setAudio.bind(this),
      setAudioState: this.setAudioState.bind(this)
    };
  }

  /*
  |-------------------------------------------------------------------------------
  | Controls
  |-------------------------------------------------------------------------------
  */

  /**
   * Создание элемента "аудио"
   * @param {*} payload
   */
  setAudio() {
    const audioElement = document.getElementById('player-controls-audio');
    this.setState({
      controlsState: {
        ...this.state.controlsState,
        el: audioElement
      }
    });
    audioElement.volume = this.state.controlsState.volume;
  }

  /**
   * Изменение состояния трека
   * @param {Object} payload
   */
  setAudioState(payload) {
    this.setState({
      controlsState: { ...this.state.controlsState, ...payload }
    });
  }

  /*
  |-------------------------------------------------------------------------------
  | Search
  |-------------------------------------------------------------------------------
  */

  /**
   * Установка состояния поискового компонента активен/неактивен
   * @param {*} state
   * @param {Boolean} focus
   */
  setFocusOnSearchState(focus) {
    this.setState({
      searchState: {
        ...this.state.searchState,
        isFocusOnSearch: focus
      }
    });
  }

  /**
   * Обновление запроса на сортировку
   * @param {*} event
   */
  updateQuery(event) {
    this.setState({
      searchState: {
        ...this.state.searchState,
        query: event.target.value
      }
    });
  }

  /**
   * Очистка запроса на сортировку
   * @param {*} state
   */
  clearQuery() {
    this.setState({
      searchState: {
        ...this.state.searchState,
        isFocusOnSearch: false,
        query: ''
      }
    });
  }

  /*
  |-------------------------------------------------------------------------------
  | Tracklist
  |-------------------------------------------------------------------------------
  */

  /**
   * Установка id текущего трека
   * @param {Number} index - индекс трека
   */
  setCurrentTrackId(index) {
    this.setState({
      tracklistState: {
        ...this.state.tracklistState,
        currentTrackId: index
      }
    });
  }

  /**
   * Получение метаданных из треков
   *
   * TODO: убрать дублирование треков
   */
  getMetadata() {
    const tracklistWithMetadata = this.state.tracklistState.tracklist;
    tracklistWithMetadata.forEach((item, index) => {
      new Promise((resolve, reject) => {
        new jsmediatags.Reader(document.location.href + item.link).read({
          onSuccess: response => {
            item.metadata = response;
            resolve(response);
          },
          onError: error => {
            reject(error);
          }
        });
      })
        .then(() => {
          this.setState({
            tracklistState: {
              ...this.state.tracklistState,
              tracklist: tracklistWithMetadata
            }
          });
        })
        .then(() => {
          const audio = document.createElement('audio');
          audio.setAttribute('src', item.link);
          audio.addEventListener('loadedmetadata', () => {
            const currentItem = this.state.tracklistState.tracklist[index];
            currentItem.duration = audio.duration;
            this.setState({
              tracklistState: {
                ...this.state.tracklistState,
                tracklist: [...this.state.tracklistState.tracklist, currentItem]
              }
            });
          });
        })
        .catch(error => {
          console.log(error);
        });
    });
  }

  render() {
    return (
      <section className="player-container">
        <header
          className={classNames({
            'player-container__controls-wrap': true,
            'player-container__controls-wrap--hidden': this.state.searchState
              .isFocusOnSearch
          })}
        >
          <PlayerControls
            controlsState={this.state.controlsState}
            controlsMethods={this.controlsMethods}
            tracklistState={this.state.tracklistState}
            tracklistMethods={this.tracklistMethods}
          />
        </header>
        <section
          className={classNames({
            'player-container__search-wrap': true,
            'player-container__search-wrap--active': this.state.searchState
              .isFocusOnSearch
          })}
        >
          <PlayerSearch
            searchState={this.state.searchState}
            searchMethods={this.searchMethods}
          />
        </section>
        <main className="player-container__tracklist-wrap">
          <PlayerTracklist
            tracklistState={this.state.tracklistState}
            tracklistMethods={this.tracklistMethods}
            searchState={this.state.searchState}
            searchMethods={this.searchMethods}
            controlsState={this.state.controlsState}
            controlsMethods={this.controlsMethods}
          />
        </main>
      </section>
    );
  }
}

export default PlayerContainer;
