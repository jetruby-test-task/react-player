/* eslint react/prefer-stateless-function: 0 */
import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';

class PlayerSearch extends React.Component {
  render() {
    return (
      <section className="player-search">
        <div className="player-search__content-wrap">
          <input
            className="player-search__field"
            type="search"
            placeholder="Search for artists or tracks"
            onChange={this.props.searchMethods.updateQuery}
            value={this.props.searchState.query}
            onFocus={() => {
              this.props.searchMethods.setFocusOnSearchState(true);
            }}
          />
          <button
            className={classNames({
              'player-search__button': true,
              'player-search__button--active': this.props.searchState
                .isFocusOnSearch
            })}
            type="button"
            onClick={() => {
              this.props.searchMethods.clearQuery();
            }}
          />
        </div>
      </section>
    );
  }
}

PlayerSearch.propTypes = {
  searchState: PropTypes.shape({
    isFocusOnSearch: PropTypes.bool.isRequired,
    query: PropTypes.string.isRequired
  }).isRequired,
  searchMethods: PropTypes.shape({
    setFocusOnSearchState: PropTypes.func.isRequired,
    updateQuery: PropTypes.func.isRequired,
    clearQuery: PropTypes.func.isRequired
  }).isRequired
};

export default PlayerSearch;
